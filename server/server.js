const express = require ("express")
const mongoose = require("mongoose")



const app = express();

mongoose.connect(
	"mongodb+srv://lizchuang:lizchuang@nosqlsession-yfu2l.mongodb.net/kairos_event_place_db?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useFindAndModify: false,
		useUnifiedTopology: true

	}
)

mongoose.connection.once("open", () => {
	console.log("Now connected to the online MongoDB server")
})

const server = require("./queries/queries.js");


server.applyMiddleware({
	app,
	// path: "/batch43"
})



app.listen(8000, () => {
  console.log(`🚀  Server ready at http://localhost:8000${server.graphqlPath}`);
})


