import React, { useState, useEffect } from "react";
import '../App.css';
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Redirect, Link } from 'react-router-dom';


// components
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import Footer from "../components/Footer";

// query
import {getVenuesQuery} from "../queries/queries";
import {getVenueQuery} from "../queries/queries";


// mutation
import {deleteVenueMutation} from "../queries/mutations";
import {updateVenueMutation} from "../queries/mutations";

const UpdateVenue = (props) =>{

	const venueData = props.getVenueQuery.getVenue ? props.getVenueQuery.getVenue:[]

	// hooks
	const [name, setName] = useState("");
	const [floorArea, setFloorArea] = useState("");
	const [maxGuest, setMaxGuest] = useState("");
	const [price, setPrice] = useState("");
	const [description, setDescription] = useState("");
	const [deleteSuccess, setDeleteSuccess] = useState(false);


	useEffect(()=>{
    	console.log(name)
    	console.log(floorArea)
    	console.log(maxGuest)
    	console.log(price)
    	console.log(description)
    })


    if (!props.getVenueQuery.loading) {
		const setDefaultValues = () => {
			// console.log(member.teamId);
			setName(venueData.name);
			setFloorArea(venueData.floorArea);
			setMaxGuest(venueData.maxGuest);
			setPrice(venueData.price);
			setDescription(venueData.description);

		};

		if (name === "") {
			setDefaultValues();
			// console.log("teamId value after setDefault: " + teamId);
		}
	}


    const nameChangeHandler = e => {
		setName(e.target.value);
	};

	const floorAreaChangeHandler = e => {
		setFloorArea(e.target.value);
	};

	const maxGuestChangeHandler = e => {
		setMaxGuest(e.target.value);
	};

	const priceChangeHandler = e => {
		setPrice(e.target.value);
	};

	const descriptionChangeHandler = e => {
		setDescription(e.target.value);
	};



  const formSubmitHandler= e =>{
  	e.preventDefault();

  	let updateVenue ={
  		id: props.match.params.id,
  		name: name,
  		floorArea: floorArea,
  		maxGuest: maxGuest,
  		price: price,
  		description: description
  	};
  	console.log(updateVenue);

  	props.updateVenueMutation({
  		variables: updateVenue
  	})

  	Swal.fire({
  		title: "Venue updated",
  		icon:"success",
  		html: '<a href="/" class="button is-success">Go back to Venues </a>',
			showCancelButton: false,
			showConfirmButton: false
  	});
  };
	

	const deleteVenueHandler = e => {
		console.log("deleting a venue...");
		console.log(e.target.id);
		let id = props.match.params.id;

		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteVenueMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getVenuesQuery
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"The venue has been deleted.",
					"success"
				);
				setDeleteSuccess(true);
			}
		});
	};

	if (!deleteSuccess) {
		console.log("something went wrong...");
	} else {
		console.log("successful delete");
		return <Redirect to="/" />;
	}


	return (
		<section >
			<Banner />

			<Nav />

			
			<div>

			 	<div className="container my-3 py-5">
					<h1 className="text-center my-2">Update Venue</h1>

					<form onSubmit={formSubmitHandler}
						className=" mx-auto">
						<div className="row">
							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="name">Venue Name:</label>
									<input 
										className="form-control" 
										type="text" 
										id="name" 
										onChange= {nameChangeHandler}
										value={name}
										/>
								</div>
							</div>	

							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="floorArea">Floor Area:</label>
									<input 
										className="form-control" 
										type="text" 
										id="floorArea"
										onChange= {floorAreaChangeHandler}
										value= {floorArea}
										/>
								</div>
							</div>	

									
						</div>

						<div className="row">
							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="maxGuest">Capacity:</label>
									<input 
										className="form-control" 
										type="text" 
										id="maxGuest"
										onChange= {maxGuestChangeHandler}
										value= {maxGuest}
										/>
								</div>
							</div>	

							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="price">Rate:</label>
									<input className="form-control" type="text" 
									id="price"
									onChange= {priceChangeHandler}
									value= {price} 
										 />
								</div>
							</div>	
			
						</div>

						<div className="row">
							<div className="col-12">
								<label htmlFor="description">Description:</label>
								<textarea
									className="form-control" 
									id="description" 
									cols="100" rows="5"
									placeholder="add here"
									onChange= {descriptionChangeHandler}
									value= {description}
										/>
								
							</div>
						</div>

							
							<button 
								className="btn btn-primary text-light w-100 mx-auto my-1"
							> Update Venue
							</button>	
						

						<Link to="">	
							<button 
								className="btn btn-danger text-light w-100 mx-auto my-1"
								id={props.match.params.id}
								onClick={
										deleteVenueHandler
										}
								color="danger"
								fullwidth> Delete Venue</button>	
						</Link>

						
					</form>
				</div>

			</div>

			
		</section>
		
		)
}


export default compose(
	graphql(updateVenueMutation, {name: "updateVenueMutation"}),
	graphql(deleteVenueMutation, {name: "deleteVenueMutation"}),
	graphql(getVenuesQuery, {name: "getVenuesQuery"}),
	graphql(getVenueQuery, { 
	options: props => {
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
	name:"getVenueQuery"})
	)(UpdateVenue);

