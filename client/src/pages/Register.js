import React, { useState, useEffect } from "react";
import '../App.css';
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Redirect } from 'react-router-dom'

// mutation
import { createUserMutation } from "../queries/mutations";

// components
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import Footer from "../components/Footer";

const Register = (props) =>{
	// console.log(props);

	// hooks
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [contactNumber, setContactNumber] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [role, setRole] = useState("");
	const [gotoLogin, setGotoLogin] = useState(false);
    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(()=>{
    	console.log(firstName)
    	console.log(lastName)
    	console.log(contactNumber)
    	console.log(email)
    	console.log(password)
    })

	const firstNameChangeHandler = e => {
		setFirstName(e.target.value);
	};

	const lastNameChangeHandler = e => {
		setLastName(e.target.value);
	};

	const contactNumberChangeHandler = e => {
		setContactNumber(e.target.value);
	};

	const emailChangeHandler = e => {
		setEmail(e.target.value);
	};

	const passwordChangeHandler = e => {
		setPassword(e.target.value);
	};

	const roleChangeHandler = e => {
		setRole(e.target.value);
	};

	const addUser = e => {
		e.preventDefault();
		

		props.createUserMutation({
			variables: {
				firstName: firstName,
				lastName: lastName,
				contactNumber: contactNumber,
				email: email,
				password: password
			}
		}).then((response) => {
			console.log(response.data)
        	const userAdded = response.data.createUser

        	localStorage.setItem("user",JSON.stringify(userAdded))
        
	        if (userAdded) {
	            Swal.fire({
	                title: 'Registration Successful',
	                text: 'You will now be redirected to the login.',
	                icon: 'success'
	            }).then(() => {
	                setGotoLogin(true)
	                // console.log("should go to login page")
	            }).catch(err=>{
	            	console.log(err)
	            })
        } else {
            Swal.fire({
                title: 'Registration Failed',
                text: 'The server encountered an error.',
                icon: 'error'
            })
        }
    })

	}	

	const checkPassword = (password) => {
        setPassword(password)

        if (password.length >= 8) {
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        }
    }

    if (gotoLogin) {
        return <Redirect to='/'/>
    }

	return (
		<section >
			<Banner />

			<Nav />

			<div className="registerBackground">

			 <div className="container py-5">
				<h1 className="registerName text-center my-3">Register</h1>

				<form onSubmit={addUser} className="w-75 mx-auto">
					<div className="row">
						<div className="col-12 col-md-6">
							<div className="form-group">
								<label htmlFor="firstName">First Name</label>
								<input 
									className="form-control" 
									type="text" 
									id="firstName"
									onChange={firstNameChangeHandler}
									value={firstName}

									/>
							</div>
						</div>	

						<div className="col-12 col-md-6">
							<div className="form-group">
								<label htmlFor="lastName">Last Name</label>
								<input 
									className="form-control" 
									type="text" 
									id="lastName"
									onChange={lastNameChangeHandler}
									value={lastName}

									/>
							</div>
						</div>					
					</div>

					<div className="row">
						<div className="col-12">
							<div className="form-group">
								<label htmlFor="email">Email</label>
								<input 
									className="form-control" 
									type="text" 
									id="email"
									onChange={emailChangeHandler}
									value={email}

									/>
							</div>
						</div>	
					</div>

					<div className="row">
						<div className="col-12">
							<div className="form-group">
								<label htmlFor="contactNumber">Contact Number</label>
								<input 
									className="form-control" 
									type="text" 
									id="contactNumber"
									onChange={contactNumberChangeHandler}
									value={contactNumber}


									/>
							</div>
						</div>					
					</div>

					<div className="row">
						<div className="col-12">
							<div className="form-group">
								<label htmlFor="password">Password</label>
								<input 
									className="form-control" 
									type="password" 
									id="password"
									onChange={passwordChangeHandler}
									value={password}


									/>
							</div>
						</div>	
					</div>

					<div className="row">
						<div className="col-12">
							<div className="form-group">
								<label htmlFor="confirmPassword">Confirm Password</label>
								<input className="form-control" type="password" id="confirmPassword"/>
							</div>
						</div>	

						<button type="submit" className="btn btn-primary text-light mx-auto">
						Register
						</button>			
					</div>
				
					

				</form>				

			</div>
		</div>

			
		</section>	
	)

};


export default compose(graphql(createUserMutation, { name: "createUserMutation"}))(Register);

