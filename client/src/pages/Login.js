import React, { useState, useEffect} from "react";
import '../App.css';
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Redirect } from 'react-router-dom';

// components
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import Footer from "../components/Footer";

import {loginUserMutation} from "../queries/mutations";

const Login = (props) =>{

	// hook
	const[email, setEmail] = useState("");
	const[password, setPassword]= useState("");
	const [gotoHome, setGotoHome] = useState(false);

console.log(props)
	useEffect(() => {
		console.log("value of email: " + email);
		console.log("value of password: " + password);
		
	})

	const emailChangeHandler = e => {
		console.log(e.target.value)
		setEmail(e.target.value)
	}

	const passwordChangeHandler = e => {
		console.log(e.target.value)
		setPassword(e.target.value)
	}


	const loginUser = e =>{
		e.preventDefault();

		props.loginUserMutation({
			variables:{
				email: email,
    			password: password
			}
		})
		.then(response=>{
			console.log(response)
			
			let user = response.data.logInUser
			console.log(user)

			localStorage.setItem("user", JSON.stringify(user))

			if(user===null){
				 Swal.fire({
                title: 'Login Failed',
                text: 'Please check your credentials',
                icon: 'error'
            })

			}else{

				setGotoHome(true)
				Swal.fire({
	                title: 'Login Successful',
	                icon: 'success'
	            })
			}

	 
		})
	}
	if (gotoHome) {
        return <Redirect to='/'/>
    }
	return (
		<section >
			<Banner />

			<Nav />

			
			<div className="loginBackground">

			 	<div className="container py-5">
					<h1 className="loginName text-center my-5">Log In</h1>

					<form onSubmit={loginUser} className="w-75 mx-auto">
						<div className="row">
							<div className="col-12">
								<div className="form-group">
									<label htmlFor="email">Email</label>
									<input 
										className="form-control" 
										type="text" 
										id="email"
										onChange={emailChangeHandler}
										value={email}
										/>
								</div>
							</div>	

							<div className="col-12">
								<div className="form-group">
									<label htmlFor="password">Password</label>
									<input 
										className="form-control" 
										type="password" 
										id="password"
										onChange={passwordChangeHandler}
										value={password}
										/>
								</div>
							</div>	

							<button type="submit" className="btn btn-primary text-light mx-auto">Login</button>				
						</div>
					</form>
				</div>

			</div>

			<Footer />
		</section>
		
		)
}

export default compose(graphql(loginUserMutation, {name: "loginUserMutation"}))(Login);


