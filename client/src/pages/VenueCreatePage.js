import React, { useState, useEffect } from "react";
import '../App.css';
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Redirect } from 'react-router-dom';

// components
import Banner from "../components/Banner";
import Nav from "../components/Nav";

// mutation
import { createVenueMutation } from "../queries/mutations";

// query
import {getVenuesQuery} from "../queries/queries"

const VenueCreatePage = (props) =>{

	// hooks
	const [name, setName] = useState("");
	const [floorArea, setFloorArea] = useState("");
	const [maxGuest, setMaxGuest] = useState("");
	const [price, setPrice] = useState("");
	const [description, setDescription] = useState("");
	const [gotoLogin, setGotoLogin] = useState(false);
    const [isDisabled, setIsDisabled] = useState(true);


	useEffect(()=>{
    	console.log(name)
    	console.log(floorArea)
    	console.log(maxGuest)
    	console.log(price)
    	console.log(description)
    })

    const nameChangeHandler = e => {
		setName(e.target.value);
	};

	const floorAreaChangeHandler = e => {
		setFloorArea(e.target.value);
	};

	const maxGuestChangeHandler = e => {
		setMaxGuest(e.target.value);
	};

	const priceChangeHandler = e => {
		setPrice(e.target.value);
	};

	const descriptionChangeHandler = e => {
		setDescription(e.target.value);
	};

	const addVenue = e => {
		e.preventDefault();
		

		props.createVenueMutation({
			variables: {
				name: name,
				floorArea: parseInt(floorArea),
				maxGuest: parseInt(maxGuest),
				price: parseInt(price),
				description: description
			},

			refetchQueries:[{
				query: getVenuesQuery
			}]

		}).then((response) => {
			console.log(response.data)
        	const venueAdded = response.data.createVenue
        
        if (venueAdded) {
            Swal.fire({
                title: 'Created Venue Successfully',
                icon: 'success'
            }).then(() => {
                setGotoLogin(true)
                // console.log("should go to login page")
            }).catch(err=>{
            	console.log(err)
            })
        } else {
            Swal.fire({
                title: 'Failed',
                icon: 'error'
            })
        }
    })

	}	

	if(gotoLogin){
		return <Redirect to="/" />
	}
	
	return (
		<section >
			<Banner />

			<Nav />

			
			<div>

			 	<div className="container my-3 py-5">
					<h1 className="text-center my-2">Create Venue</h1>

					<form onSubmit= {addVenue} className=" mx-auto">
						<div className="row">
							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="name">Venue Name:</label>
									<input 
										className="form-control" 
										type="text" 
										id="name" 
										onChange= {nameChangeHandler}
										value={name}
										/>
								</div>
							</div>	

							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="floorArea">Floor Area:</label>
									<input 
										className="form-control" 
										type="text" 
										id="floorArea"
										onChange= {floorAreaChangeHandler}
										value= {floorArea}
										/>
								</div>
							</div>	

									
						</div>

						<div className="row">
							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="maxGuest">Capacity:</label>
									<input 
										className="form-control" 
										type="text" 
										id="maxGuest"
										onChange= {maxGuestChangeHandler}
										value= {maxGuest}
										/>
								</div>
							</div>	

							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="price">Rate:</label>
									<input className="form-control" type="text" 
									id="price"
									onChange= {priceChangeHandler}
									value= {price}
										 />
								</div>
							</div>	
			
						</div>

						<div className="row">
							<div className="col-12">
								<label htmlFor="description">Venue Description:</label>
								<textarea
									className="form-control" 
									id="description" 
									cols="100" rows="5"
									placeholder="add here"
									onChange= {descriptionChangeHandler}
									value= {description}
										/>
								
							</div>
						</div>

						<button type="submit" className="btn btn-primary text-light w-100 mx-auto my-1">Add Venue</button>	
						
					</form>
				</div>

			</div>

			
		</section>
		
		)
}

export default compose(
	graphql(createVenueMutation, { name: "createVenueMutation"}),
	graphql(getVenuesQuery, { name: "getVenuesQuery"})
	)(VenueCreatePage);
