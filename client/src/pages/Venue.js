import React, {useState,useEffect} from "react";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import {Link} from "react-router-dom";
import '../App.css';

// components
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import Hero from "../components/Hero";

import Footer from "../components/Footer";

// query
import {getVenueQuery} from "../queries/queries";

const Venue = (props) =>{
console.log(props)
	const venueData = props.getVenueQuery.getVenue ? props.getVenueQuery.getVenue:[]
	console.log(venueData)
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));

	return (
		<section >
			      <Banner />

			        <Nav />

			        <Hero />
			        <div className="venue-content">
						<div className="container">
							<div className="row" >

								<div className="col-12 col-md-5  bg-light my-5 shadow-lg p-0">
						 			<h1 className="venue-name text-center">{venueData.name}</h1>

						 			<h4 className="text-center">Venue Inclusions</h4>
						 			<ul>
						 				<li>{venueData.description}</li>

						 			</ul>

						 			<h4 className="text-center">Details</h4>
						 			<ul>
						 				<li>Floor Area: {venueData.floorArea} sqm</li>
						 				<li>Capacity: {venueData.maxGuest} pax</li>
						 				<li>Rate: {venueData.price} pesos</li>
						 			</ul>
						 		 <div className="mx-auto text-center">

								

								{user && user.role==="user" ?  
        						(
           							<Link to={`/book-venue/${venueData.id}` }>
				   						<a className="book-venue btn btn-primary text-light">Book Venue</a>
									</Link>
          						):""}
								
								{user && user.role==="admin" ?  
        						(
           							<Link to={`/update-venue/${venueData.id}` }>
				   						<a className="book-venue btn btn-warning text-light">Update Venue</a>
									</Link>
          						):""}


								</div>

								</div>

								<div className="col-12 col-md-7 my-5">
									<div className="row">
										<div className="col-12 col-md-6">
											<img className="img-fluid shadow-lg" src="/images/img1.jpeg" alt=""/>
										</div>

										<div className="col-12 col-md-6">
											<img className="img-fluid my-5 shadow-lg" src="/images/img2.jpeg" alt=""/>

										</div>


									</div>


									<div className="row">
										<div className="col-12 col-md-6">
											<img className="img-fluid shadow-lg" src="/images/img3.jpeg" alt=""/>
										</div>

										<div className="col-12 col-md-6">
											<img className="img-fluid shadow-lg" src="/images/img4.jpg" alt=""/>

										</div>

									</div>
								</div>
						
							</div>
		
						</div>
					</div>

			    	
		</section>
		
		)
}

export default compose(graphql(getVenueQuery, { 
	options: props => {
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
	name:"getVenueQuery"}))(Venue);
