import React, { useState, useEffect} from "react";
import '../App.css';
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Redirect, Link } from 'react-router-dom';

// components
import Banner from "../components/Banner";
import Nav from "../components/Nav";

// mutation
import { updateCateringServiceMutation } from "../queries/mutations";
import { deleteCateringServiceMutation } from "../queries/mutations";

// query
import { getCateringServiceQuery } from "../queries/queries";
import { getCateringServicesQuery } from "../queries/queries";


const UpdateCateringService = (props) =>{
	console.log(props)

	const cateringServiceData = props.getCateringServiceQuery.getCateringService
		? props.getCateringServiceQuery.getCateringService
		: [];

		// console.log(cateringService);


	// hooks
	const [packageName, setPackageName] = useState("");
	const [pricePerHead, setPricePerHead] = useState("");
	const [description, setDescription] = useState("");
	const [menu, setMenu] = useState("");
	const [deleteSuccess, setDeleteSuccess] = useState(false);
	// const [updateSuccess, setUpdateSuccess] = useState(false);


	useEffect(()=>{
		console.log("value of packageName " + packageName);
		console.log("value of pricePerHead " + pricePerHead);
		console.log("value of description " + description);
		console.log("value of menu " + menu);

	})

	if (!props.getCateringServiceQuery.loading) {
		const setDefaultCatering = () => {
			// console.log(cateringService.packageName);
			setPackageName(cateringServiceData.packageName);
			setPricePerHead(cateringServiceData.pricePerHead);
			setDescription(cateringServiceData.description);
			setMenu(cateringServiceData.menu);
		};

		if (packageName === "") {
			setDefaultCatering();
			// console.log("cateringService value after setDefault: " + cateringService);
		}
	}

	const packageNameChangeHandler = e => {
		setPackageName(e.target.value);
	};

	const pricePerHeadChangeHandler = e => {
		setPricePerHead(e.target.value);
	};

	const descriptionChangeHandler = e => {
		setDescription(e.target.value);
	};

	const menuChangeHandler = e => {
		setMenu(e.target.value);
	};



  const formSubmitHandler= e =>{
  	e.preventDefault();

  	let updateCateringService ={
  		id: props.match.params.id,
  		packageName: packageName,
  		pricePerHead: pricePerHead,
  		description: description,
  		menu: menu
  	};
  	console.log(updateCateringService);

  	props.updateCateringServiceMutation({
  		variables: updateCateringService
  	})

  	Swal.fire({
  		title: "catering service updated",
  		icon:"success",
  		html: '<a href="/catering" class="button is-success">Go back to Catering Services </a>',
			showCancelButton: false,
			showConfirmButton: false

  	});
  };

  const deleteCateringServiceHandler = e => {
		console.log("deleting a catering service...");
		console.log(e.target.id);
		let id = props.match.params.id;

		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteCateringServiceMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getCateringServicesQuery
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"The catering package has been deleted.",
					"success"

				);
				setDeleteSuccess(true);
			}
		});


	};

	if (!deleteSuccess) {
		console.log("something went wrong...");
	} else {
		console.log("successful login");
		return <Redirect to="/catering" />;
	}


	return (
		<section >
			<Banner />

			<Nav />

			
			<div>

			 	<div className="container my-3 py-5">
					<h1 className="text-center my-2">Update Catering Service</h1>

					<form onSubmit={formSubmitHandler}
						className=" mx-auto">
						<div className="row">
							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="packageName">Catering Package Name:</label>
									<input 
										className="form-control" 
										type="text" 
										id="packageName"
										onChange={packageNameChangeHandler}
										value={packageName}
										
										/>
								</div>
							</div>	

							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="pricePerHead">Price per Head: </label>
									<input 
										className="form-control" 
										type="text" 
										id="pricePerHead"
										onChange={pricePerHeadChangeHandler}
										value={pricePerHead}
							
										/>
								</div>
							</div>	

									
						</div>


						<div className="row">
							<div className="col-12">
								<label htmlFor="cateringServiceDescription">Description:</label>
								<textarea 
									className="form-control" 
									id="cateringServiceDescription" 
									cols="100" 
									rows="5"
									onChange={descriptionChangeHandler}
									value={description}
									>
								</textarea>
							</div>
						</div>

						<div className="row">
							<div className="col-12">
								<label htmlFor="menu">Menu:</label>
								<input 
									className="form-control" 
									type="text" 
									id="menu" 
									placeholder="add here"
									onChange={menuChangeHandler}
									value={menu}
									/>
		
							</div>
						</div>

						
							<button 
								className="btn btn-primary text-light w-100 mx-auto my-1"
								fullwidth> Update Catering Service
							</button>	
						
						
							<button
								type="button" 
								className="btn btn-danger text-light w-100 mx-auto my-1"
								id={props.match.params.id}
								onClick={
										deleteCateringServiceHandler
										}
								color="danger"
								fullwidth> Delete Catering Service
							</button>	
						
							
					</form>
				</div>

			</div>

			
		</section>
		
		)
}

export default compose(
			graphql(updateCateringServiceMutation, {name: "updateCateringServiceMutation"}),
			graphql(deleteCateringServiceMutation, {name: "deleteCateringServiceMutation"}),
			graphql(getCateringServicesQuery, {name: "getCateringServicesQuery"}),	
			graphql(getCateringServiceQuery, {
				options: props => {
					console.log(props.match.params.id);
						return {
							variables: {
							id: props.match.params.id
						}
					};
				},
				name: "getCateringServiceQuery"})
	)(UpdateCateringService);

