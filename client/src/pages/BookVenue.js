import React, { useState, useEffect } from "react";
import '../App.css';
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Redirect } from 'react-router-dom';
import {Link} from "react-router-dom";

// components
import Banner from "../components/Banner";
import Nav from "../components/Nav";

// query
import {getVenueQuery, getCateringServicesQuery, getTransactionsQuery } from "../queries/queries";

// mutation
import {createTransactionMutation} from "../queries/mutations";



const BookVenue = (props) =>{
	console.log(props)
	// hooks
	const [venue, setVenue] = useState(props.match.params.id);
	const [catering, setCatering]= useState("");
	const [date, setDate] = useState("");
	const [event, setEvent] = useState("");
	const [time, setTime] = useState("");
	const [guest, setGuest] = useState("");
	

	useEffect(() => {
		console.log("value of venue: " + venue);
		console.log("value of catering: " + catering);
		console.log("value of date: " + date);
		console.log("value of event: " + event);
		console.log("value of time: " + time);
		console.log("value of guest: " + guest);
	})

	const venueChangeHandler = e => {
		console.log(e.target.value)
		setVenue(e.target.value)
	}

	const dateChangeHandler = e => {
		console.log(e.target.value)
		setDate(e.target.value)
	}

	const eventChangeHandler = e => {
		console.log(e.target.value)
		setEvent(e.target.value)
	}

	const cateringChangeHandler = e => {
		console.log(e.target.value)
		setCatering(e.target.value)
	}

	const timeChangeHandler = e => {
		console.log(e.target.value)
		setTime(e.target.value)
	}

	const guestChangeHandler = e => {
		console.log(e.target.value)
		setGuest(e.target.value)
	}





	const venueData = props.getVenueQuery.getVenue ? props.getVenueQuery.getVenue:[]


	let cateringServicesData = props.getCateringServicesQuery.getCateringServices 
		? props.getCateringServicesQuery.getCateringServices
		:[]
	
	console.log(cateringServicesData)


	const cateringServiceOptions = () => {
			
		if (props.getCateringServicesQuery.loading) {
			return <option>Loading catering service...</option>;
		} else {
			return cateringServicesData.map(cateringService => {
				return (
					<option key={cateringService.id} value={cateringService.id}>
						{cateringService.packageName}
					</option>
				);
			});
		}
	};

	const addTransaction = e =>{
		e.preventDefault();

		let newTransaction ={
				venueId: venue,
    			eventType: event,
   				guestNumber: Number(guest),
    			cateringService: catering,
    			date: Date(date),
    			time: time
		};
		console.log(newTransaction);

		props.createTransactionMutation({
			variables: newTransaction,

			refetchQueries: [
			{
				query: getTransactionsQuery
			}]
		}).then(res=>{
			console.log(res.data)
		}).catch(err=>{
			console.log(err)
		});

		Swal.fire({
			position: "center",
			icon: "success",
			title: "Successful",
			showConfirmButton: false,
			timer: 1500 
		});

		setVenue("");
		setCatering("");
		setDate("");
		setTime("");
		setGuest("");


	}
	
	return (
		<section >
			<Banner />

			<Nav />


			 	<div className="container my-3 py-5">
					<h1 className="text-center my-2">Book a Venue</h1>

					<form onSubmit={addTransaction}>
						<div className="row">
							<div className="col-md-6 mx-auto">
								<div className="form-group">
									<label htmlFor="name">Venue Name:</label>
									<input 
										className="form-control" 
										type="text" 
										id="name" 
										value={venueData.name}
										/>
								</div>
							</div>	
						</div>
						<div className="row">
							<div className="col-md-6 mx-auto">
								<div className="form-group">
									<label htmlFor="eventType">Event Type:</label>
									<select name="eventType" id="eventType" className="w-100" onChange={eventChangeHandler} value={event}>
										<option value="birthday">Birthday</option>
										<option value="kiddieBirthday">Kiddie Birthday</option>
										<option value="debut">Debut</option>
										<option value="baptismal">Baptismal</option>
										<option value="corporate">Corporate</option>
										<option value="wedding">Wedding</option>
									</select>
								</div>
							</div>	
						</div>

						<div className="row">
							<div className="col-md-6 mx-auto">
								<div className="form-group">
									<label htmlFor="guest">Number of Guests:</label>
									<input 
										className="form-control" 
										type="number" 
										id="guest" 
										onChange={guestChangeHandler}
										value={guest}
									
										/>
								</div>
							</div>	
						</div>

						<div className="row">
							<div className="col-md-6 mx-auto">
								<div className="form-group">
									<label htmlFor="cateringService">Catering Service:</label>
									<select
												// onChange={cateringServiceChangeHandler}
											className="w-100" onChange={cateringChangeHandler} value={catering}>
												<option disabled selected>
													Select cateringService
												</option>
												{cateringServiceOptions()}
											</select>

								</div>
							</div>	
						</div>
						<div className="row">
							<div className="col-12 col-md-6 mx-auto">
								<div className="form-group">
									<label htmlFor="date">Date of Event:</label>
									<input className="form-control" type="date" 
									id="date"
									onChange={dateChangeHandler}
									value={date}
									
										 />
								</div>
							</div>	
			
						</div>

						<div className="row">
							<div className="col-12 col-md-6 mx-auto">
								<label htmlFor="time">Time:</label>
								<input 
									className="form-control" 
									type="text" 
									id="time" 
									onChange={timeChangeHandler}
									value={time}

									
								/>
								
							</div>
						</div>
						<div className="row">
							<button type="submit" className="btn btn-primary text-light w-50 col-md-6 mx-auto mt-3">Book</button>	
						</div>
					</form>
				</div>

			
		</section>
		
		)
}

export default compose(
	graphql(getCateringServicesQuery, {name:"getCateringServicesQuery"}),
	graphql(createTransactionMutation, {name:"createTransactionMutation"}),
	graphql(getTransactionsQuery, {name:"getTransactionsQuery"}),
	graphql(getVenueQuery, { 
	options: props => {
			console.log(props.match.params.id);
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
	name:"getVenueQuery"}))(BookVenue);

