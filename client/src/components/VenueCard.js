import React from "react";
import {Link} from "react-router-dom";

import '../App.css'

const VenueCard = (props) =>{
	console.log(props)
	return (
		<div>
			<div className="card shadow" style={{width: '18rem'}}>
			  <img src={props.venueImage} className="card-img-top img-fluid venue-image" alt="..." />
			  <div className="card-body">
			    <h1 className="card-title">{props.name}</h1>
			    <p className="card-text">{props.description}</p>

				<Link to={props.link}>
			   		 <a className="btn btn-primary text-light">More Details</a>
				</Link>
			  </div>
			</div>
		</div>
		
		)
}

export default VenueCard;
