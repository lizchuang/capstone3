import React from "react";

import '../App.css'

const Hero = () =>{
	return (
		<div className="image-container">
			<img src="/images/hero2.png" alt="garden image" className="hero-image" align="bottom" />
		</div>
		
		)
}

export default Hero;